function createCard(name, description, pictureUrl, startDate, endDate, loc) {

    return `
      <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
        <div class="card shadow p-3 mb-5 bg-body rounded">
          <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${loc}</h6>
            <p class="card-text">${description}</p>
            <div class="card-footer">
            ${startDate.toLocaleDateString('en-US')} - ${endDate.toLocaleDateString('en-US')}
            </div>
          </div>
        </div>
      </div>
    `;
  }

function createPlaceholderCard() {
return `
    <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
        <div class="card shadow p-3 mb-5 bg-body rounded">
            <div class="card-img-top placeholder-glow">
                <span class="placeholder col-12" style="height:200px;"></span>
            </div>
            <div class="card-body placeholder-glow">
                <h5 class="card-title placeholder col-6"></h5>
                <h6 class="card-subtitle mb-2 text-muted placeholder col-4"></h6>
                <p class="card-text placeholder col-7"></p>
                <p class="card-text placeholder col-4"></p>
                <p class="card-text placeholder col-4"></p>
                <p class="card-text placeholder col-6"></p>
                <p class="card-text placeholder col-8"></p>
            </div>
        </div>
    </div>
`;
}

function createError() {
  return `
  <div class="alert alert-primary" role="alert">
    A simple primary alert—check it out!
  </div>
`
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
      if (!response.ok) {
          throw new Error("Something went wrong with data.")
      }
  
      const data = await response.json();
      const column = document.querySelector('.row');
      column.innerHTML = data.conferences.map(() => createPlaceholderCard()).join('');
  
      for (let i = 0; i < data.conferences.length; i++) {
        const conference = data.conferences[i]
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);
          const endDate = new Date(details.conference.ends);
          const loc = details.conference.location.name;
          const html = createCard(title, description, pictureUrl, startDate, endDate, loc);
  
          column.children[i].outerHTML = html;
        }
      }
  
    } catch (e) {
      console.error("An error occurred:", error);
      column.innerHTML = createError(error.message);
    }
  });
